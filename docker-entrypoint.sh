#!/bin/sh

echo "Get group and id"
$userAndGroup=$(ls -l ${DEV_SRC_PATH} | awk '{print $3,$4}' | uniq | tr --delete "\n")
echo ${userAndGroup}

cd ${DEV_SRC_PATH}

if [-z package.json]; then
  npm install
  if [ $# -eq 0 ]; then
   npm start
  else
    exec $@
  fi
else
  echo "Seems path is not a valid node.js project"
  exit 1
fi
