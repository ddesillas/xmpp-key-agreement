FROM node:8-slim

#Default values to ovveridden in order to map existing user to another user
ARG DEV_HOME="/home/developer"
ARG DEV_SRC_PATH="${DEV_HOME}/code"

RUN DEBIAN_FRONTEND=noninteractive apt-get update &&\
    apt-get -y install libgtkextra-dev libgconf2-dev libnss3 libasound2 libxtst-dev libxss1 &&\
    apt-get clean && rm -rf /var/lib/apt/lists/ &&\
    npm install -g electron-forge &&\
    npm cache clean --force

COPY docker-entrypoint.sh /usr/bin/entrypoint.sh

RUN chmod +x /usr/bin/entrypoint.sh &&\
    mkdir -p ${DEV_SRC_PATH} &&\
    useradd -d ${DEV_HOME} -s /bin/bash developer &&\
    chown -R developer:developer ${DEV_HOME}

VOLUME ${DEV_SRC_PATH}

ENTRYPOINT ["/usr/bin/entrypoint.sh"]
