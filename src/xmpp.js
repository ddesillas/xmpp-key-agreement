const { client, jid, xml } = require('@xmpp/client');
const { Roster } = require('./xmpp_helpers/roster');
const GroupChat = require('./xmpp_helpers/group_chat');
const presence_handler = require('./xmpp_helpers/presence_handler');
const { ChatRoomStatus } = require('./xmpp_helpers/chatroom_info');

let clientInstance = null;
// Connection tries
let connectionCounter = 5;

// Indicator whether the login logic had been executed
let loginProcessed = false;

// The logedin address
let loginAddress = null;

// The list of the friends
const roster = new Roster();

// Handler gor group Instance
let groupChatInstance = null;

// Indication whether presence has been sent
let presenceSent = false;

let ChatRoomStatusInstance = new ChatRoomStatus();

/**
 * Unitilty for sending a message
 * @param {xml} message
 * @param {Function} errorCallback
 */
const sendMessage = (message, errorCallback) => {
  if (clientInstance) {
    clientInstance.send(message).catch((err) => {
      console.error('Send Error', err.toString());
      if (errorCallback && typeof errorCallback === 'function') {
        errorCallback(err);
      }
    });
  }
};

/**
 * Handle when user sends a presence message
 * @param {xml} stanza The message received from xmpp server
 * @param {*} webContents
 * @returns Boolean in order to determine whether to proceed or not
 */
const presenceReply = (stanza, webContents) => {
  if (!stanza.is('presence')) return false;
  // Handling of the multichat here
  const extentionInfo = stanza.getChild('x');
  if (typeof stanza.getChild('x') === 'object') {
    // Handle it as normal presence
    if (typeof extentionInfo.attrs.xmlns === 'undefined') { return presence_handler.handleNormalPresence(stanza, loginAddress, roster, webContents); }
    switch (extentionInfo.attrs.xmlns) {
      case presence_handler.roomCreationXmlns():
        if (groupChatInstance) {
          return groupChatInstance.roomCreationOrErrorHandlingMessage(stanza, webContents, loginAddress);
        }
        return false;
      default:
        return presence_handler.handleNormalPresence(stanza, loginAddress, roster, webContents);
    }
  } else {
    return presence_handler.handleNormalPresence(stanza, loginAddress, roster, webContents);
  }
};

/**
* Check if user is loged in
*/
const isLogedIn = () => clientInstance !== null && clientInstance.status === 'online';

/**
 * Request to fetch your friends
 */
const rosterRequest = () => {
  try {
    const message = xml('iq', {
      type: 'get',
      from: loginAddress.bare().toString(),
    }, xml('query', { xmlns: 'jabber:iq:roster' }));
    sendMessage(message);
  } catch (e) {
    console.error('Roster Error', e.message);
  }
};

const handleMessageStanza = (stanza, webContents) => {
  if (!stanza.is('message')) return false;
  // Handling of the multichat here
  if (typeof stanza.attrs.type === 'string' && stanza.attrs.type === 'groupchat') return groupChatInstance.handleGroupMessage(stanza, webContents);
  // Other Message Handling Goes There
  const extentionInfo = stanza.getChild('x');
  if (typeof stanza.getChild('x') === 'object' && typeof extentionInfo.attrs.xmlns === 'string') {
    switch (extentionInfo.attrs.xmlns) {
      case groupChatInstance.invitationXMLNS():
        if (groupChatInstance) {
          return groupChatInstance.handleInvitation(stanza, webContents, roster, loginAddress);
        }
        return false;
      default:
       return false;
    }
  } else {
    return false;
  }

};

/**
 * Send that the current user is online
 * @param {String} toAddr (Optional) The address to send from
 */
const sendStatusOnline = () => {
  if (!presenceSent) {
    const message = xml('presence',
      { to: loginAddress.getDomain() },
      xml('show', {}, 'chat'),
    );
    setTimeout(() => {
      sendMessage(message, err => console.log('error'));
    }, 1500);
    presenceSent = true;
  }
};

/**
 * Fetching roster
 * @param {xml} stanza
 * @param {*} webContents
 */
const handleRoster = (stanza, webContents) => {
  if (!stanza.is('iq')) return false;
  if (typeof stanza.attrs === 'undefined' || typeof stanza.attrs.type === 'undefined') return false;
  if (stanza.attrs.type !== 'result' && stanza.attrs.type !== 'set') return false;

  try {
    const query = stanza.getChild('query');

    if (!query) {
      // console.log('Iq not roster');
      return false; // let other methogs handle other types of iq
    }

    if (query.attrs.xmlns === 'jabber:iq:roster') {
      const participants = query.getChildren('item');
      if (participants) {
        participants.forEach((element) => {
          const group = element.getChildText('group');
          switch (element.attrs.subscription) {
            case 'both':
              roster.addFriend(group, element.attrs.jid, element.attrs.name);
              break;
            case 'from':
              roster.addFriend(group, element.attrs.jid, element.attrs.name);
              sendMessage(xml('presence', { to: element.attrs.jid, type: 'subscribe' }));
              break;
            case 'to':
              // console.log('Subscription to', element.attrs.jid);
              roster.addFriend(group, element.attrs.jid, element.attrs.name);
              sendMessage(xml('presence', { to: element.attrs.jid, type: 'subscribed' }));
              break;
            case 'none':
              // Remove the friend from roster?
              break;
            default:
              // Do nothing just to make eslint to shut the f* up.
          }
        });
      }
      webContents.send('roster', roster.getRoster());
    } else {
      // webContents.send('roster', roster.getRoster());
      return false;
    }
  } catch (e) {
    console.error('Roster Error', e.toString());
  }
};

const pingReply = (stanza) => {
  if (!stanza.is('iq')) return false;
  if (typeof stanza.attrs === 'undefined' || typeof stanza.attrs.type === 'undefined') return false;
  if (stanza.attrs.type !== 'result' && stanza.attrs.type !== 'get') return false;

  try {
    const query = stanza.getChild('ping');

    if (!query || (typeof query.attrs !== 'undefined' && typeof query.attrs.xmlns !== 'undefined' && query.attrs.xmlns !== 'urn:xmpp:ping')) {
      // console.log('Iq not ping');
      return false; // let other methogs handle other types of iq
    }
    const message = xml('iq', { to: stanza.attrs.from, type: 'result' });
    sendMessage(message);
  } catch (e) {
    console.error('Pong', e);
  }
};

/**
 * Generate the connect connection string
 * @param {String} url The connection url
 */
const sanitizeServerUrl = function (url) {
  let newUrl = '';
  if (!/^xmpp(s)?:\/\//.test(url)) {
    newUrl = `xmpp://${url}`;
  }
  return newUrl;
};

/**
 * Object of regular expressions
 * That match various library generated errors
 */
const errorRegex = {
  DNS_NOT_FOUND: /^getaddrinfo ENOTFOUND/,
  AUTH_FAILED: /^not-authorized/,
};

/**
 * Shuts down the client and resetes the parameters.
 * @param {*} xmpp
 * @param {Boolean} completeDestruction
 */
const destructClient = (xmpp, completeDestruction) => {
  if (xmpp) {
    sendMessage(xml('presence', { type: 'unavailable' }));
    xmpp.stop();
  }

  if (completeDestruction) {
    clientInstance = null;
    connectionCounter = 5;
    loginProcessed = false;
    loginAddress = '';
    presenceSent = false;
    roster.reset();
  }
};

/**
* Bootstrap xmpp client
* @param {Object} xmpp The xmpp client
* @param {Object} ipcMain The interpocess communicator
* @param {DialogsHelper} dialogsHelper The helper to create the dialogs
*/
const initXmpp = (xmpp, dialogsHelper, webContents) => {
  xmpp.on('error', (err) => {
    if (errorRegex.DNS_NOT_FOUND.test(err.message)) {
      dialogsHelper.errorDialog('Connection Error', `The provided server ${loginAddress.getDomain()} does not exist.`);
      webContents.send('disconnected', false, 'server');
      destructClient(xmpp);
    } else if (errorRegex.AUTH_FAILED.test(err.message)) {
      dialogsHelper.errorDialog('Authentication Error', 'The provided username and password are not valid');
      webContents.send('disconnected', false, 'credentials');
      destructClient(xmpp);
    } else if (err.code === 'ECONNREFUSED' && connectionCounter === 0) {
      dialogsHelper.errorDialog('Connection Error', `Failed to connect into ${err.address}.`);
    }
  });

  xmpp.on('offline', () => {
    console.log('Disconnected');
    webContents.send('disconnected', true);
    destructClient(xmpp, true);
  });

  xmpp.on('status', (status) => {
    console.log(status);
  });

  xmpp.on('online', (address) => {
    if (isLogedIn() && !loginProcessed) {
      loginProcessed = true;
      connectionCounter = 10;
      loginAddress = address;
      webContents.send('login_ok', loginAddress.toString());
      rosterRequest();
    }
  });

  xmpp.on('stanza', (stanza) => {
    const messageString = stanza.toString();
    try {
      if (new RegExp('<iq\s*.*>\s*<bind xmlns="urn:ietf:params:xml:ns:xmpp-bind">.*<\/bind><\/iq>').test(messageString)) return;
      if (presenceReply(stanza, webContents) !== false) return;
      // eslint-disable-next-line no-useless-return
      if (handleRoster(stanza, webContents) !== false) return;
      if (pingReply(stanza) !== false) return;
      if (handleMessageStanza(stanza, webContents) !== false) return;
    } catch (e) {
      console.error('Stanza Processing', e.toString());
      console.error('Error Trace', e.stack);
    }
  });

  xmpp.reconnect.on('reconnecting', () => {
    if (connectionCounter === 0) {
      destructClient(xmpp);
    }
    connectionCounter -= 1;
  });

  // if (process.env.NODE_ENV && process.env.NODE_ENV === 'dev') {
  //   xmpp.on('input', (input) => {
  //     console.debug('⮈', input);
  //   });
  //   xmpp.on('output', (output) => {
  //     console.debug('⮊', output);
  //   });
  // }

  // We use then and catch for better error handling and for the process to run smoothly
  xmpp.start().then((s) => {
    console.log(s);
  }).catch((e) => {
    console.error('Debug error', e.toString(), e.message);
    console.error(e.stack);
  });
};

/**
 * Main xmpp connection constructor and handler
 * @param mainWindow {Object} The main window
 * @param ipcMain {Object} A way to send events to the ui
 * @param dialogsHelper {Object} Helper to proviude common ways to handle dialogs
 */
module.exports = function XmppCli(mainWindow, ipcMain, dialogsHelper) {
  ipcMain.on('login', (event, params) => {
    if (clientInstance) {
      destructClient(clientInstance);
    }

    const jidVal = jid(params.username);
    const serverUrl = sanitizeServerUrl(params.server);

    const connectionInfo = {
      service: serverUrl,
      domain: jidVal.getDomain(),
      username: jidVal.getLocal(),
      password: params.password,
    };

    groupChatInstance = new GroupChat(sendMessage, serverUrl, ChatRoomStatusInstance);

    // eslint-disable-next-line new-cap
    clientInstance = new client(connectionInfo);
    initXmpp(clientInstance, dialogsHelper, mainWindow.webContents);
  });

  ipcMain.on('isLogedIn', (event) => {
    // console.log(clientInstance);
    if (isLogedIn()) {
      event.returnValue = loginAddress.toString();
    } else {
      event.returnValue = false;
    }
  });

  ipcMain.on('logout', () => {
    this.disconnect();
  });

  ipcMain.on('acceptFriend', (event, friend) => {
    // console.log('Friend Accepted', friend);
    sendMessage(xml('presence', { to: friend, type: 'subscribed' }));
  });

  ipcMain.on('rejectFriend', (event, friend) => {
    // console.log('Friend rejected', friend);
    sendMessage(xml('presence', { to: friend, type: 'unsubscribed' }));
  });

  ipcMain.on('key_agreement', (event, jids) => {
    groupChatInstance.createRoomSendMessage(loginAddress, jids);
  });

  ipcMain.on('triggerOnline', () => {
    sendStatusOnline();
  });

  this.disconnect = () => {
    destructClient(clientInstance, true);
  };
};
