/**
 * GKE+P Group Key Areeement Impementation
 */

const _ = require('underscore');
const fill = require('lodash.fill');
const crypto = require('crypto');
const spawn = require('threads').spawn;

/**
 * If an array is not defined generates a new array with predefined values
 * @param {Array} array The array whether will be checks if is full or not
 * @param {Integer} size The size of the array
 * @param {Integer} value The value to fill
 */
const initArrayWithValue = (array, size, value) => {
  if (!_.isArray(array) || _.isEmpty(array)) {
    const arrayToReturn = Array(size);
    fill(arrayToReturn, value);
    return arrayToReturn;
  }
  return array;
};

const createBufferfromValue = (buffer, value) => {
  if (_.isEmpty(buffer)) {
    return Buffer.from([value]);
  }
  return Buffer.concat([buffer, Buffer.from([value])]);
};

/**
 * @param {Array[Buffer]} buffers The list of the buffers.
 * @param {Array | null | undefined} bytesReadPerBuffer Used on recursion counts how many chunlks read from buffer.
 * @param {Array | null | undefined} canReadStatus Used On recursion indicates whether can get another byte from the buffer.
 * @param {Function} callback
 */
const xorBuffers = (buffers, callback, bytesReadPerBuffer = null, canReadStatus = null, xoredValue = null) => {
  if (!_.isFunction(callback)) throw new Error('Callback should be a function');
  const buffersListSize = _.size(buffers);
  if (buffersListSize === 0) return; // Do not waste resourses on recursion method calls and loops

  let immutableBytesReadPerBuffer = null;
  if(bytesReadPerBuffer) {
    immutableBytesReadPerBuffer = _.map(bytesReadPerBuffer, value => value);
  } else {
    immutableBytesReadPerBuffer = _.map(buffers, buffer => buffer.length-1);
  }

  const immutableCanReadStatus = initArrayWithValue(canReadStatus, buffersListSize, true);

  const value = _.reduce(buffers, (memo, buffer, key) => {
    if (immutableCanReadStatus[key]) {
      const cursorToRead = immutableBytesReadPerBuffer[key];
      const byte = buffer.readUInt8(cursorToRead);
      const newMemo = memo ^ byte;
      immutableBytesReadPerBuffer[key] = cursorToRead - 1;
      immutableCanReadStatus[key] = immutableBytesReadPerBuffer[key] >= 0;
      return newMemo;
    }
    return memo^0;
  }, 0);

  // Place the value in the buffer
  const immutableXoredValue = createBufferfromValue(xoredValue, value);

  const callCallback = _.every(immutableCanReadStatus, booleanValue => booleanValue === false);
  // Either the recursion or the callback is called in async in order to reduce the stack and call the method.
  if (callCallback) {
    return process.nextTick(() => callback(immutableXoredValue));
  }
  // Call recursion
  return process.nextTick(() => xorBuffers(buffers, callback, immutableBytesReadPerBuffer, immutableCanReadStatus, immutableXoredValue));
};


const dhStringParameterToBuffer = (parameter) => {
  if (_.isString(parameter)) return Buffer.from(parameter, 'hex');
  return parameter;
};

/**
 * Generate the keys and the diffie hellman key agreement object.
 * @param {Integer} p The prime for Diffie Hellman Key Generation
 * @param {Integer} g The generator for Diffie Hellman Key Exchange
 * @param {Function} callback The callback in order to provide the keys and the diffie-hellman Object.
 */
const createSelfKey = (p, g, callback) => {
  const thread = spawn((input, done) => {
    const { createDiffieHellman } = require('crypto');
    const keypair = require('keypair');
    const pVal = input.p;
    const gVal = input.g
    let dh = null;

    if (pVal && gVal) {
      dh = createDiffieHellman(pVal, 'hex', gVal,'hex');
    } else {
      console.log('Generating Diffie Hellman');
      dh = createDiffieHellman(2048);
    }

    console.log('Generating Agreement Keys');
    const pubKey = dh.generateKeys('hex');

    console.log('Generating Signature Keys');
    const sigKeys = keypair();

    done({
      prime: dh.getPrime().toString('hex'),
      generator: dh.getGenerator().toString('hex'),
      pubKey,
      privKey: dh.getPrivateKey('hex'),
      signaturePubKey: sigKeys.public,
      signaturePrivateKey: sigKeys.private,
    });
  });

  return thread.send({p, g}).on('message', (response) => {
    // Recreating Diffie Hellman Object because I cannot pass aroutnd through a threaded function
    const dh = crypto.createDiffieHellman(dhStringParameterToBuffer(response.prime), dhStringParameterToBuffer(response.generator));
    dh.setPublicKey(response.pubKey);
    dh.setPrivateKey(response.privKey);
    const pubkey = dhStringParameterToBuffer(response.pubKey);
    callback(dh, pubkey, response.signaturePubKey, response.signaturePrivateKey);
    thread.kill();
  }).on('error', (err) => {
    console.error(err);
    thread.kill();
  });
};

/**
 * Perform a sha256 data hashing.
 * @param {Buffer} data
 */
const hashData = (data, suid) => {
  const hash = crypto.createHash('sha256');
  hash.update(`${data.toString('hex')},${suid}`);
  return hash.digest();
};

/**
 * Hash a Bufferlist
 * @param {Buffer[]} list 
 */
const hashBufferList = (buffers) => {
  const hash = crypto.createHash('sha256');
  _.each(buffers, (buffer)=> hash.update(buffer));
  return hash.digest();
};

/**
 * Signing a Message for group Key Agreement.
 * @param {String} nick
 * @param {Buffer} z
 * @param {String} uid
 * @param {Buffer} signatureKey
 */
const sign = (nick, z, uid, signatureKey) => {
  const signatureMechanism = crypto.createSign('RSA-SHA256');
  signatureMechanism.write(nick);
  signatureMechanism.write(z.toString('hex'));
  signatureMechanism.write(uid);
  signatureMechanism.end();

  return signatureMechanism.sign(signatureKey, 'hex');
};

/**
 * Verifies a received keypair message.
 * @param {string} nick 
 * @param {string} z 
 * @param {string} uid 
 * @param {string} verificationKey 
 * @param {string} signature
 * @return {Boolean}
 */
const verify = (nick, z, uid, signature, verificationKey) => {
  const verificationMechanism = crypto.createVerify('RSA-SHA256');
  verificationMechanism.write(nick);
  verificationMechanism.write(z.toString('hex'));
  verificationMechanism.write(uid);
  verificationMechanism.end();

  return verificationMechanism.verify(verificationKey, signature, 'hex');
};

const xoredBuffersAreZero = (buffers, callback) => {
  xorBuffers(buffers, (value) => {
    console.log(value);
    const zeroBuffer = Buffer.alloc(value.length, 0);
    return callback(zeroBuffer.compare(value) === 0);
  });
};

module.exports.createSelfKey = createSelfKey;
module.exports.xorBuffers = xorBuffers;
module.exports.hashData = hashData;
module.exports.hashBufferList = hashBufferList;
module.exports.sign = sign;
module.exports.verify = verify;
module.exports.xoredBuffersAreZero = xoredBuffersAreZero;
