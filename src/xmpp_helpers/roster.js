const _ = require('underscore');
const { jid } = require('@xmpp/client');

/* eslint-disable no-param-reassign */

/**
 * A utility Function for concating a value to an uknown indec
 * @param {String} friend
 * @param {String} resource
 */
const arrayfy = (toBearray, value) => {
  if (toBearray && toBearray.constructor && toBearray.constructor.name === 'Array') {
    return toBearray.concat(value);
  }
  return [value];
};

/**
 * Item that will be iside the roster
 * @param {String} jid The participant Id
 */
function RosterItem(jidValue, userName) {
  const jidObj = jid(jidValue);
  const name = userName;
  const jidString = jidObj.bare().toString();
  let status = 'offline';

  const self = this;

  if (jidObj.getResource()) {
    resources.concat(jidObj.getResource());
  }

  /**
   * Gets whether participant is online or not.
   * @returns {String}
   */
  self.getStatus = () => status;

  /**
   * Set availability status
   * @param {String} newStatus The status of the participant
   */
  self.setStatus = (newStatus) => {
    // eslint-disable-next-line no-self-assign
    status = newStatus;
  };

  /**
   * Append an addictional Resource
   * @param {String} resource The resource of the participant
   */
  self.appendResource = (resource) => {
    resources.concat(resource);
  };

  self.getResources = () => resources;

  /**
   * @returns {String} With the particioants jid
   */
  self.getJid = () => jidString;

  /**
   * Returns true if a participant has jid
   * @param jid {String || jid || RosterItem} The id of the other participant
   * @returns {Boolean}
   */
  self.equals = (jidValue) => {
    if (jidValue instanceof RosterItem) {
      return jidValue.getJid() === jidString;
    } else if (jidValue instanceof jid) {
      return jidValue.bare().equals(jidString);
    }
    return jidValue === jidString;
  };

  /**
   * Generating the Object needed for rendering the template
   * @return {Object}
   */
  self.serialiseForRendering = () => ({ name, jid: jidString, status: self.getStatus() });
}

/**
 * A singleton for creating and managing the roster
 */

function Roster() {
  let roster = {};
  const self = this;

  self.updatePresenceStatus = (jid, status, callback) => {
    self.findFriendInRoster(jid, (friend, index, group) => {
      friend.setStatus(status);
      if (callback && typeof callback === 'function') {
        callback(self.getRoster());
      }
    });
  };

  /**
   * Adds a friend to the roster
   * @param {String} friend The roster friend.
   * @param {String} group The group that your friend is.
   */
  self.addFriend = (group, jid, name) => {
    self.findFriendInRoster(jid, null, () => {
      const user = new RosterItem(jid, name);
      if (!group) group = 'Unsorted Friends';
      roster[group] = arrayfy(roster[group], user);
    });
  };

  /**
   * Search a Friend in the roster and perform an action to the callback.
   * @param {string} friend The jid of the friend
   * @param {Function} callback Callback method that is called when the friend is found. If not found thet is not called at all.
   * @param {Function} callbackNotFound Callback that is called when item not found.s
   */
  self.findFriendInRoster = (friend, callback, callbackNotFound) => {
    const key = _.findKey(roster, (val, key) => {
      const index = _.findIndex(val, item => item.equals(friend));
      if (index >= 0) {
        if (callback) callback(roster[key][index], index, key);
        return key;
      }
    });
    if (!key && callbackNotFound) {
      callbackNotFound();
    }
  };

  /**
   * Remove a friend From the roster.
   * @param {String} friend The friend you want to remove.
   * @param {Function} callback (Optional) Function called when friend has been removed.
   */
  self.removeFriend = (friend, callback) => {
    self.findFriendInRoster(friend, (friend, index, group) => {
      const participant = roster[group][index];
      roster[group] = _.reject(roster[group], el => el.equals(participant));
      if (roster[group].length === 0) {
        delete roster[group];
      }
      if (callback && typeof callback === 'function') {
        callback(self.getRoster(), participant);
      }
    });
  };


  /**
   * Empties the roster removing all friends
   * NOTICE: That method does not offers a callback to notify
   * when all friend has been removed.
   */
  self.reset = () => { roster = {}; };

  self.getRoster = () => {
    const returnedRoster = {};
    _.forEach(roster, (value, key) => {
      returnedRoster[key] = _.map(value, val => val.serialiseForRendering());
    });
    return returnedRoster;
  };
}

module.exports.Roster = Roster;
module.exports.RosterItem = RosterItem;
