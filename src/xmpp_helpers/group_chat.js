const { xml, jid } = require('@xmpp/client');
const _ = require('underscore');
const { ParticipantStatus } = require('./chatroom_info');
const GroupKeyAgreement = require('./key_agreement');

/**
 * Class that handles the chatroom messages.
 * @param {Function} sendMessageFunction The required function to send the message.
 * @param {String} serverUrl The url of the server.
 * @param {ChatRoomStatus} ChatroomWithParticipants Object used to store participant info. 
 */
function ChatRoom(sendMessageFunction, serverUrl, ChatroomWithParticipants) {
  const self = this;

  const groupXmlns = 'http://jabber.org/protocol/muc';

  const newUrl = serverUrl.replace(/^xmpp(s)?:\/\//, '').replace(/:\d*$/, '');
  // Common convention on most servers. I sticked to that because of demo.
  const chatroomService = `conference.${newUrl}`;

  if (!sendMessageFunction || typeof sendMessageFunction !== 'function') {
    const error = new Error('Missing Parameters');
    error.code = 'DEPEDENCY_MISSING';
  }

  const KeyAgreement = new GroupKeyAgreement(sendMessageFunction, ChatroomWithParticipants);

  const generateChatroomFromJid = (chatroomjid) => {
    return jid(chatroomjid).getLocal();
  };

  self.invitationXMLNS = () => 'http://jabber.org/protocol/muc#user';

  /**
   * @param {String} chatroom 
   */
  const inviteFriendsToChatroom = (chatroom) => {
    const friends = ChatroomWithParticipants.getChatroomFriends(chatroom);
    const invitation = xml('message', { to: `${chatroom}@${chatroomService}` });
    const extention = xml('x', { xmlns: `${groupXmlns}#user` });
    _.forEach(friends, (friend) => {
      if (friend.creator === false) {
        extention.append(xml('invite', { to: friend.jid }));
      }
    });
    invitation.append(extention);
    sendMessageFunction(invitation);
  };

  /**
   * Send the message to create a new Room
   * @param {jid.JID} chatRoomOwner The user to send the message
   * @param {Array} friends The friends that will join to this room
   */
  self.createRoomSendMessage = (chatRoomOwner, friends) => {
    const chatroomName = 'keygen' + Date.now();
    ChatroomWithParticipants.associateChatroomwithFriends(chatroomName, friends);
    ChatroomWithParticipants.setGeneratedFromLogedinUser(chatroomName);
    ChatroomWithParticipants.setCreator(chatroomName, chatRoomOwner.toString(), chatRoomOwner.getLocal());
    const message = xml('presence', { from: chatRoomOwner.toString(), to: `${chatroomName}@${chatroomService}/${chatRoomOwner.getLocal()}` }, xml('x', { xmlns: groupXmlns }));
    sendMessageFunction(message);
  };

  /**
   * Logic for joining into a room
   * @param {String} chatRoomFullJid The room to join.
   * @param {String} user The user that joins to the room.
   */
  self.joinToARoom = (chatRoomFullJid, user) => {
    ChatroomWithParticipants.associateChatroomwithFriends(generateChatroomFromJid(chatRoomFullJid), [jid(user).bare().toString()], ParticipantStatus.JOINED);
    const nick = jid(user).getLocal();
    const message = xml('presence', { to: `${chatRoomFullJid}/${nick}`, from: user }, xml('x', { xmlns: groupXmlns }));
    sendMessageFunction(message);
  };

  /**
   * Logic when an incvited friend is asked to join into a room.
   * @param {String} chatroomName 
   * @param {String} participantJoined 
   * @param {*} webContents 
   */
  const handleJoin = (chatroomName, roomFullJid, participantJoined, webContents, isCreatedFromlogedinUser) => {
    if (!isCreatedFromlogedinUser) {
      ChatroomWithParticipants.associateChatroomwithFriends(chatroomName, [participantJoined], ParticipantStatus.JOINED);
      webContents.send('joined', chatroomName, participantJoined);
    } else {
      ChatroomWithParticipants.setParticipantStatus(chatroomName, participantJoined, ParticipantStatus.JOINED, (participants) => {
          KeyAgreement.sendSelfKeys(roomFullJid, chatroomName);
          ChatroomWithParticipants.setRoomMetadata(chatroomName, 'keysSent', true);
      });
    }
    // .... Dev Zone ....
    // ChatroomWithParticipants.iterateSorted(chatroomName, (element, previous, next, key, list) => {
    //   console.log("################### DEBUG #####################");
    //   console.log("Element", element);
    //   console.log("Next", next);
    //   console.log("Previous", previous);
    //   console.log("Key", key);
    //   console.log("List", list);
    //   console.log("################### /DEBUG #####################");
    // });
  };

  /**
   * @param {xml} stanza The message that Indicates Sucessfull creation or room or not.
   * @param {*} webContents The event emmiter to contact eith the ui.
   * @param {jid} logedinUser The user that has been logged in
   */
  self.roomCreationOrErrorHandlingMessage = (stanza, webContents, logedinUser) => {
    if (!stanza.is('presence')) return false;
    const extention = stanza.getChild('x');
    if (!extention) return false;
    if (typeof stanza.attrs === 'undefined' || typeof extention.attrs === 'undefined' || typeof extention.attrs.xmlns === 'undefined') return false;
    if (extention.attrs.xmlns !== `${groupXmlns}#user`) return false;

    const item = extention.getChild('item');
    if (!item) return false;
    const chatroomName = generateChatroomFromJid(stanza.attrs.from);

    const isCreatedFromlogedinUser = ChatroomWithParticipants.isGeneratedFromLogedinUser(chatroomName);

    if (typeof item.attrs !== 'undefined' && typeof item.attrs.jid === 'string' && !jid(item.attrs.jid).bare().equals(logedinUser.bare())) {
      return handleJoin(chatroomName, stanza.attrs.from, jid(item.attrs.jid).bare().toString(), webContents, isCreatedFromlogedinUser);
    }

    if (!isCreatedFromlogedinUser) return;

    // Message processing code goes Bellow here.
    if (typeof stanza.attrs.type !== 'undefined' && stanza.attrs.type === 'error') {
      ChatroomWithParticipants.deleteChatRoom(chatroomName);
      return webContents.send('roomCreation', false, chatroomName);
    }

    const message = xml('iq', { to: stanza.attrs.from, from: stanza.attrs.to, type: 'set' });
    message.append(xml('query', { xmlns: `${groupXmlns}#owner` }, xml('x', { xmlns: 'jabber:x:data', type: 'submit' })));
    sendMessageFunction(message);
    webContents.send('roomCreation', true, chatroomName);
    inviteFriendsToChatroom(chatroomName);
  };

  /**
   * Hanbdle and respond to XEP-0045 Invitations for group Chat
   * @param {xml} stanza
   * @param {*} webContents
   * @param {Roster} roster
   */
  self.handleInvitation = (stanza, webContents, roster, loginAddress) => {
    if (!stanza.is('message')) return false;
    
    const extention = stanza.getChild('x');
    if (!extention || typeof extention.attrs === 'undefined' || typeof extention.attrs.xmlns === 'undefined' || extention.attrs.xmlns !== `${groupXmlns}#user` ) return false;
    
    const invitation = extention.getChild('invite');
    if (!invitation || typeof invitation === 'undefined' ) return false;
    
    if (typeof invitation.attrs.from === 'string') {
      roster.findFriendInRoster(invitation.attrs.from, () => {
        self.joinToARoom(stanza.attrs.from, loginAddress.toString());
      });
    }
  };

  self.handleGroupMessage = (stanza, webContents) => {
    if (!stanza.is('message')) return false;
    const body = stanza.getChild('body');
    if (!body) return false; // All Group messages should be in a body tag.
    const extention = body.getChild('x');
    if (extention) {
      if (!_.isString(extention.attrs.xmlns)) return false; // Extentions should have xmlns attribute
      
      switch (extention.attrs.xmlns) {
        case 'http://pcmagas.tk/gkePlusp#intiator_key':
         return KeyAgreement.onKeyMessageReceival(stanza, webContents);
        case 'http://pcmagas.tk/gkePlusp#second_phase':
          return KeyAgreement.onPhaseReceival(stanza, webContents);
      }
    }
  };
}

module.exports = ChatRoom;
