const { jid } = require('@xmpp/client');

module.exports = {
  handleNormalPresence: (stanza, loginAddress, roster, webContents) => {
    try {
      const tmpFrom = jid(stanza.attrs.from);
            // const fromResource = tmpFrom.getResource();
      const from = tmpFrom.bare().toString();

      if (loginAddress.bare().equals(from)) return false;

      if (typeof stanza.attrs !== 'undefined' && typeof stanza.attrs.type !== 'undefined') {
        switch (stanza.attrs.type) {
          case 'subscribe':
            webContents.send('friendRequest', from);
            break;
          case 'unavailable':
                  // webContents.send('friensStatus', from, 'offline');
            roster.updatePresenceStatus(from, 'offline', (roster) => {
              webContents.send('roster', roster);
            });
            break;
          case 'unsubscribe': // A user breaks the friendship status
            roster.removeFriend(from, (newRosterStatus) => {
              webContents.send('roster', newRosterStatus);
            });
            break;
        }
      } else {
              // Status change
        let status = stanza.getChildText('show');
        if (!status) {
          status = 'chat';
        }
        roster.updatePresenceStatus(from, status, (roster) => {
          webContents.send('roster', roster);
        });
      }
    } catch (e) {
      console.error('Presence Reply', e.message);
    }
  },
  roomCreationXmlns: () => "http://jabber.org/protocol/muc#user",
};
