const { xml, jid } = require('@xmpp/client');
const _ = require('underscore');
const et = require('elementtree');
const { ParticipantStatus } = require('./chatroom_info');

/**
 * Logic For Sending messages in to a chatroom
 * @param {Object} participant Object for the participant information in the Chatroom
 * @param {Object} dh
 * @param {String} chatroomJid The chatroom address
 * @param {Function} sendMessageCallback The callback for sending messages
 */
const createKeyMessage = (participant, dh, chatroomJid, sendMessageCallback) => {
  const chatroom = jid(chatroomJid).bare().toString();
  const message = xml('message', { to: chatroom, from: participant.jid, type: 'groupchat' });
  const extention = xml('x', { xmlns: 'http://pcmagas.tk/gkePlusp#intiator_key' });
  extention.append(xml('p', {}, dh.getPrime().toString('hex')));
  extention.append(xml('g', {}, dh.getGenerator().toString('hex')));
  extention.append(xml('encryptionkey', {}, participant.pubKey.toString('hex')));  
  extention.append(xml('signaturekey', {}, participant.signatureKey.toString('hex')));
  message.append(xml('body', {}, extention));
  sendMessageCallback(message);
};

/**
 * Send the prossessed information after message has been sent.
 * @param {String} uid 
 * @param {Buffer} z 
 * @param {String} signature 
 * @param {String} from 
 * @param {*} to 
 * @param {*} sendMessageCallback 
 */
const sendKeyAgreementSecondPhaseMessage = (uid, z, signature, from, chatroomJid, sendMessageCallback) => {
  const chatroom = jid(chatroomJid).bare().toString();
  const message = xml('message', { to: chatroom, from, type: 'groupchat' });
  const extention = xml('x', { xmlns: 'http://pcmagas.tk/gkePlusp#second_phase' });
  extention.append(xml('uid', {}, uid));
  extention.append(xml('z', {}, z.toString('hex')));
  extention.append(xml('signature', {}, signature));
  const body = xml('body', {}, extention)
  message.append(body);
  sendMessageCallback(message);
};

/**
 * Handling the message Exchange for group Key agreement
 * @param {Function} sendMessageCallback
 * @param {ChatRoomStatus} ChatroomWithParticipants
 */
function GroupKeyAgreement(sendMessageCallback, ChatroomWithParticipants) {
  const self = this;
  /**
   * Send the Owner participant Keys into the Chatroom
   */
  self.sendSelfKeys = (chatroomJid, chatroomName) => {
    ChatroomWithParticipants.generateCreatorKeys(chatroomName, (creator, group) => {
      createKeyMessage(creator, group.dh, chatroomJid, sendMessageCallback);
    });
  };

  self.onKeyMessageReceival = (stanza, webContents) => {
    if (!stanza.is('message')) return false;
    if (ChatroomWithParticipants.isSameUserFromUserJid(stanza.attrs.to, stanza.attrs.from)) return false; // Ignore messages send from herself.
    const extention = stanza.getChild('body').getChild('x');
    if (!extention) return false;
    if (!_.isString(extention.attrs.xmlns) || extention.attrs.xmlns !== 'http://pcmagas.tk/gkePlusp#intiator_key') return false;
    // Checking if the message provides p and g
    const chatroomFullJid = jid(stanza.attrs.from);
    const nick = chatroomFullJid.getResource();
    const chatroom = chatroomFullJid.getLocal();
    
    console.log(stanza.attrs.to, "Received Key message from", nick);
    
    const p = extention.getChildText('p');
    const g = extention.getChildText('g');

    //Using elementtree because we can parse message better

    const pubKey = extention.getChildText('encryptionkey');
    const signatureKey = extention.getChildText('signaturekey');
    const jidBare = jid(stanza.attrs.to).bare().toString();

    ChatroomWithParticipants.geneRateKeysUponReceival(chatroom, stanza.attrs.to, p, g, (friend, chatroomObj) => {
      if(_.has(chatroomObj,'keyMessageSent') && chatroomObj.keyMessageSent) return;
      createKeyMessage(friend, chatroomObj.dh, chatroomFullJid.bare().toString(), sendMessageCallback);
      chatroomObj.keyMessageSent = true;

      // Placing duplicate code to shave time and fix a bug that each user does not send their key agreement params.
      // Recommended refactor: Set a method that sends to each user their keys.
      ChatroomWithParticipants.setParticipantThaisIsNotAChatroomCreatorItsKeys(chatroom, nick, pubKey, signatureKey, () => {
        ChatroomWithParticipants.keyGenHandler(chatroom, jidBare, (z, uid, signature, chatroom) => {
          sendKeyAgreementSecondPhaseMessage(uid, z, signature, jidBare, stanza.attrs.from, sendMessageCallback);
          chatroom.keyAgreementParamsSent = true;
        });
      });
    });

    ChatroomWithParticipants.setParticipantThaisIsNotAChatroomCreatorItsKeys(chatroom, nick, pubKey, signatureKey, () => {
      ChatroomWithParticipants.keyGenHandler(chatroom, jidBare, (z, uid, signature, chatroom) => {
        sendKeyAgreementSecondPhaseMessage(uid, z, signature, jidBare, stanza.attrs.from, sendMessageCallback);
        chatroom.keyAgreementParamsSent = true;
      });
    });
  };

  self.onPhaseReceival = (stanza, webContents) => {
    if (!stanza.is('message')) return false;
    if (ChatroomWithParticipants.isSameUserFromUserJid(stanza.attrs.to, stanza.attrs.from)) return false; // Ignore messages send from herself.
    const extention = stanza.getChild('body').getChild('x');
    if (!extention) return false;
    if (!_.isString(extention.attrs.xmlns) || extention.attrs.xmlns !== 'http://pcmagas.tk/gkePlusp#second_phase') return false;

    const chatroomFullJid = jid(stanza.attrs.from);
    const nick = chatroomFullJid.getResource();
    const chatroom = chatroomFullJid.getLocal();
    const jidBare = jid(stanza.attrs.to).bare().toString();
    const z = extention.getChildText('z');
    const uid = extention.getChildText('uid');
    const signature = extention.getChildText('signature');

    console.log(stanza.attrs.to, "Received PARAMS message from", nick);
    console.log('Has Set Keys for parameters', ChatroomWithParticipants.getChatroomMetadata(chatroom, 'keyMessageSent'));

    if (!ChatroomWithParticipants.getChatroomMetadata(chatroom, 'keyMessageSent')) {
      ChatroomWithParticipants.keyGenHandler(chatroom, jidBare, (z, uid, signature, chatroom) => {
        console.log(stanza.attrs.to, "Sending Keys when received PARAMS message");
        sendKeyAgreementSecondPhaseMessage(uid, z, signature, jidBare, stanza.attrs.from, sendMessageCallback);
        chatroom.keyAgreementParamsSent = true;
        console.log('Sending parameters');
        ChatroomWithParticipants.setParamsFromSecondStep(chatroom, nick, uid, z, signature, (error, status) => {
          if(!error && status) {
            ChatroomWithParticipants.generateFinalKey(chatroom, jidBare, uid, (key) => {
              console.log('Final Key', key.toString('hex'));
            });
          }
        });
      });
    } else {
      // Handle the params there
      ChatroomWithParticipants.setParamsFromSecondStep(chatroom, nick, uid, z, signature, (error, status) => {
        if (!error && status) {
          ChatroomWithParticipants.generateFinalKey(chatroom, jidBare, uid, (key) => {
            console.log('Final Key', key.toString('hex'));
          });
        }
      });
    }
  };
}

module.exports = GroupKeyAgreement;
