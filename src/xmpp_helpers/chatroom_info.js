/**
 * NOTICE: Please DO Refactor Thes piece of code it was written
 * in a time of a hurry thus it ended up like a piece of sh***.
 * 
 * Recommended refactor tasks / requirements:
 *  - Create a class for each participant:
 *    - Each participant has jid, nick, signature private key, signature public key, diffie hellman private key, diffie hellman public key, z, signature.
 *  - Create a class for chatroom:
 *    - Also should have a method that checks is all participants have same status
 *    - Function to search its participant by jid or a nickname
 *    - Function to create the uid a string that has the following format: (nick|publicKey,...,nick|publicKey)
 */
const { jid } = require('@xmpp/client');
const _ = require('underscore');
const cryptoHelpers = require('../keygen_helpers');

const ParticipantStatus = {
  INVITATION_PENDING: 'INVITATION_PENDING',
  JOINED: 'JOINED',
  GENERATING_KEYS: 'GENERATING_KEYS',
  KEYS_DEFINED: 'KEYS_DEFINED',
  PARAMETERS_CONFIGURED: 'PARAMETERS_CONFIGURED',
  SIGNATURE_ABORTED: 'SIGNATURE_ABORTED',
};

/**
 * Return the previous key from a given numerical key
 * @param {Integer} key
 * @param {Integer} size
 */
const previousKey = (key, size) => {
  if (key === 0) return size - 1;
  return key - 1;
};

/**
 * Return the next key from a given numerical key
 * @param {Integer} key
 * @param {Integer} size
 */
const nextKey = (key, size) => {
  if (key === size - 1) return 0;
  return key + 1;
};

/**
 *
 * @param {Array} list
 * @param {Int | String} sortKey
 * @param {Function} callback
 */
const iterateListSorted = (list, sortKey, callback) => {
  const listSorted = _.sortBy(list, sortKey);
  _.each(listSorted, (element, key, theList) => {
    if (_.isFunction(callback)) {
      const listSize = _.size(theList);
      const previousKeyValue = previousKey(key, listSize);
      const nextKeyValue = nextKey(key, listSize);
      return callback(element, theList[previousKeyValue], theList[nextKeyValue], key, theList);
    }
  });
};

/**
 * Used as function for keeping the status on each chat room
 */
function ChatRoomStatus() {
  /**
   * @var {Object}
   */
  const chatrooms = {};

  /**
   * Create a friend from a given string
   * @param {String} friend
   */
  const createAnIninvitedFriendStastus = (friend, owner, status) => {
    const friendJid = jid(friend);
    const object = { jid: friendJid.bare().toString(),
      creator: false,
      nick: friendJid.getLocal(),
      status: ParticipantStatus.INVITATION_PENDING,
    };

    if (owner) { // If the user is a chatroom owner
      object.creator = true;
      if (!status) {
        object.status = ParticipantStatus.JOINED;
        return object; // Seems like a wase but is not, I escape the duplicate check on status
      }
    }

    if (status) {
      object.status = status;
    }
    return object;
  };

  /**
   * Add an uninvited user to the a specific chatroom
   * @param {String} chatroom
   * @param {Object} unInvitedFriend
   */
  const appendFriend = (chatroom, unInvitedFriend) => {
    if (_.isObject(chatrooms[chatroom]) && _.has(chatrooms[chatroom], 'friends')) {
      chatrooms[chatroom].friends.push(unInvitedFriend);
    } else if (_.isObject(chatrooms[chatroom]) && !_.has(chatrooms[chatroom], 'friends')) {
      chatrooms[chatroom].friends = [unInvitedFriend];
    } else {
      chatrooms[chatroom] = {
        friends: [unInvitedFriend],
      };
    }
  };

  /**
   * Locates a friend by nick and
   * @param {String} chatroom The chatroomName
   * @param {String} friend The friend's Nickname
   * @param {Function} foundCallback Callback called if found
   * @param {Function} notForundCallback Callback called if not found
   */
  const processFriendByNick = (chatroom, friend, foundCallback, notForundCallback) => {
    const index = _.findIndex(chatrooms[chatroom].friends, friendToCheck => friendToCheck.nick === friend);
    if (index < 0) {
      if (_.isFunction(notForundCallback)) return notForundCallback();
    } else if (_.isFunction(foundCallback)) return foundCallback(chatrooms[chatroom].friends[index], chatrooms[chatroom], index);
  };

  /**
   * Locates a friend by nick and
   * @param {String} chatroom The chatroomName
   * @param {String} friend The friend's Nickname
   * @param {Function} foundCallback Callback called if found
   * @param {Function} notFoundCallback Callback called if not found
   */
  const processFriendByJid = (chatroom, friend, foundCallback, notFoundCallback) => {
    const jidBare = jid(friend).bare().toString();
    const index = _.findIndex(chatrooms[chatroom].friends, friendToCheck => friendToCheck.jid === jidBare);

    if (index < 0) {
      if (_.isFunction(notFoundCallback)) notFoundCallback();
      return;
    }

    if (_.isFunction(foundCallback)) return foundCallback(chatrooms[chatroom].friends[index], chatrooms[chatroom], index);
  };

  /**
   * Adds Unique values to a chatroom assuming that the chatroom has already been created.
   * @param {String} chatroom
   * @param {String} friend
   */
  const addUniqueToChatroom = (chatroom, friend, owner, status) => {
    const index = _.findIndex(chatrooms[chatroom], friendToCheck => friendToCheck.jid === friend);
    if (index < 0) {
      const unInvitedFriend = createAnIninvitedFriendStastus(friend, owner, status);
      if (owner) {
        unInvitedFriend.status = ParticipantStatus.JOINED;
      }
      appendFriend(chatroom, unInvitedFriend);
    }
  };

  /**
   * Set chatroom's Creator
   * @param {String} chatroom
   * @param {String} creator
   */
  this.setCreator = (chatroom, creator) => {
    addUniqueToChatroom(chatroom, creator, true);
  };

  /**
   * @param {String} user The user to check if exists in the room
   * @param {String} roomFullJid The jid of the room that we waht to check whether is same room
   */
  this.isSameUserFromUserJid = (user, roomFullJid) => {
    const roomJid = jid(roomFullJid);
    const roomName = roomJid.getLocal();
    const roomNick = roomJid.getResource();
    const userJid = jid(user).bare().toString(); // As precaution to strip the resurce
    const index = _.findIndex(chatrooms[roomName].friends, friendToCheck => friendToCheck.nick === roomNick);
    if (index < 0) return false;
    return chatrooms[roomName].friends[index].jid === userJid;
  };

  /**
   * This Method fetched the creator of the Chatroom and executes a callback on it.
   * @param {String} chatroom The chatroom to fetch the creator
   * @param {Function} callback The callback of the chatroom.
   */
  this.processCreator = (chatroom, callback) => {
    const index = _.findIndex(chatrooms[chatroom].friends, friend => friend.creator);
    return callback(chatrooms[chatroom].friends[index], index, chatrooms[chatroom]);
  };


  /**
   * NOTICE: this function MUTATES the friend Object
   * @param {Object} friend The friend to set the cryptographic values.
   * @param {*} cryptoValues The cryptographic values.
   */
  const setCryptographicKeys = (friend, pubKey, signaturePubKey, signaturePrivKey) => {
    friend.pubKey = pubKey;
    friend.signatureKey = signaturePubKey;
    friend.signaturePrivKey = signaturePrivKey;
    friend.uidPart = `${friend.nick}|${friend.pubKey.toString('hex')}`;
    friend.status = ParticipantStatus.KEYS_DEFINED;
  };

  /**
   * Callback for handling the Key generation
   * @param {Object} dh The Diffie Hellman Object.
   * @param {Buffer} pubKey The public Key.
   * @param {Buffer} signatureKey The key for signing messages
   */
  const handleKeyGeneration = (dh, pubKey, signaturePubKey, signaturePrivKey, friend, chatroomObj, callback) => {
    process.nextTick(() => {
      chatroomObj.dh = dh;
      setCryptographicKeys(friend, pubKey, signaturePubKey, signaturePrivKey);
      callback(friend, chatroomObj);
    });
  };

  /**
   * Generate keys for the Chatroom Creator:
   * @param {String} chatroom The chatroom to fetch the creator
   * @param {Function} callback The callback of the chatroom.
   */
  this.generateCreatorKeys = (chatroom, callback) => this.processCreator(chatroom, (friend, index, chatroomObj) => {
    cryptoHelpers.createSelfKey(null, null, (dh, pubKey, signatureKey, signaturePrivKey) => {
      handleKeyGeneration(dh, pubKey, signatureKey, signaturePrivKey, friend, chatroomObj, callback);
    });
  });

  /**
   * Store Public Keys of a participant.
   * @param {String} chatroom The chatroom to fetch the creator.
   * @param {String} nick The user's nickname in a chatroom.
   * @param {String} pubKeyHex A string containing the key in Hexadimal form.
   * @param {String} signatureKeyHex A string containing the signature key in hexadimal form.
   */
  this.setParticipantThaisIsNotAChatroomCreatorItsKeys = (chatroom, nick, pubKeyHex, signatureKeyHex, callback) => {
    processFriendByNick(chatroom, nick, (friend) => {
      setCryptographicKeys(friend, Buffer.from(pubKeyHex, 'hex'), signatureKeyHex);
      callback();
    });
  };

  /**
   * Generate Keys Upor receival of P and G
   * @param {String} chatroom The chatroom name
   * @param {String} jid The jid of the user
   * @param {String} p Hex-encoded string of Primary DH parameter.
   * @param {String} g Hex-encoded string of Generator DH parameter.
   * @param {Function} callback The callback method.
   */
  this.geneRateKeysUponReceival = (chatroom, jid, p, g, callback) => {
    processFriendByJid(chatroom, jid, (friend, chatroomObj) => {
      if (friend.status === ParticipantStatus.GENERATING_KEYS) return;
      if (_.has(friend, 'pubKey') && _.has(friend, 'signatureKey')) return process.nextTick(() => callback(friend, chatroomObj));
      friend.status = ParticipantStatus.GENERATING_KEYS;
      process.nextTick(() => cryptoHelpers.createSelfKey(p, g, (dh, pubKey, signatureKey, signaturePrivateKey) => {
        handleKeyGeneration(dh, pubKey, signatureKey, signaturePrivateKey, friend, chatroomObj, callback);
      }));
    });
  };

  /**
   * Sets the status of a participant and when all participants has the smae status calls a callback
   *
   * @param {String} chatroom The chatroom Name
   * @param {String} participant The participant to set the tatus
   * @param {String} status The status to sen into a participant
   * @param {Function} allParticipantHaveSameStatus A callback method when all participantsHave same status
   */
  this.setParticipantStatus = (chatroom, participant, status, allParticipantHaveSameStatusCallback) => {
    const index = _.findIndex(chatrooms[chatroom].friends, friendToCheck => friendToCheck.jid === participant);
    if (index >= 0) {
      chatrooms[chatroom].friends[index].status = status;
      const allParticipansHaveSameStatus = _.reduceRight(chatrooms[chatroom].friends, (memo, friend) => memo && friend.status === status, true);
      if (allParticipansHaveSameStatus && _.isFunction(allParticipantHaveSameStatusCallback)) {
        allParticipantHaveSameStatusCallback(chatrooms[chatroom].friends);
      }
    }
  };

  this.getParticipantByNick = (chatroom, nick, callback) => {
    process.nextTick(() => processFriendByNick(chatroom, nick, (friend) => {
      callback(friend);
    }));
  };

  /**
   * Associate chatroom with jids
   * @param {String} chatroom
   * @param {Array} friends
   */
  this.associateChatroomwithFriends = (chatroom, friends, status) => {
    _.forEach(friends, friend => addUniqueToChatroom(chatroom, friend, false, status));
  };

  /**
   * Mark if as chatroom is generated from the logedin user.
   * @param {String} chatroom
   */
  this.setGeneratedFromLogedinUser = (chatroom) => {
    if (chatrooms[chatroom]) {
      chatrooms[chatroom].generated = true;
    } else {
      chatrooms[chatroom] = {
        generated: true,
        friends: [],
      };
    }
  };

  /**
   * Keep status whether the currently logedin user created a chatroom
   * @param {String} chatroom
   */
  this.isGeneratedFromLogedinUser = (chatroom) => {
    if (!chatrooms[chatroom]) {
      return false;
    }

    if (typeof chatrooms[chatroom].generated === 'undefined') {
      return false;
    }

    return chatrooms[chatroom].generated;
  };

  /**
   * @param {String} chatroom
   */
  this.deleteChatRoom = (chatroom) => {
    if (chatrooms[chatroom]) delete chatrooms[chatroom];
  };

  /**
   * @param {String} chatroom The chatroom name
   * @param {String} key The chatroom Key
   * @param {String} value The value fot chatroom meta
   */
  this.setChatroomMeta = (chatroom, key, value) => {
    chatrooms[chatroom].meta[key] = value;
  };

  this.getMetaValue = (chatroom, key) => chatrooms[chatroom].meta[key];

  /**
   * @param {String} chatroom
   */
  this.getChatroomFriends = chatroom => chatrooms[chatroom].friends;

  /**
   * @param {Function} callback Function called on each iteration
   */
  this.iterateSorted = (chatroom, callback) => {
    iterateListSorted(chatrooms[chatroom].friends, 'jid', callback);
  };

  /**
   * @param {Array} listSorted
   * @param {String} participantjid
   * @param {Function} callback
   */
  const getPreviousNext = (listSorted, participantjid, callback) => {
    const index = _.findIndex(listSorted, friend => friend.jid === participantjid);
    if (index >= 0) {
      process.nextTick(() => {
        const previousKeyVal = previousKey(index, listSorted.length);
        const nextKeyVal = nextKey(index, listSorted.length);
        callback(listSorted[index], listSorted[previousKeyVal], listSorted[nextKeyVal]);
      });
    }
  };

  /**
   * @param {String} chatroom The chatroom that we want to create the key
   * @param {String} jid The jid of the received message
   * @param {Function} callback Function to send the data
   */
  this.keyGenHandler = (chatroom, participantjid, callback) => {
    const nakedJid = participantjid;
    const dh = chatrooms[chatroom].dh;
    const listSorted = _.sortBy(chatrooms[chatroom].friends, 'nick');
    const allParticipantsHaveKeys = _.reduce(listSorted, (memo, friend) => memo && friend.status === ParticipantStatus.KEYS_DEFINED, true);

    if (allParticipantsHaveKeys) {
      const uid = _.reduce(listSorted, (memo, friend) => memo + friend.uidPart, '');
      console.log('Generates uid', uid);
      this.setRoomMetadata(chatroom, 'uid', uid);
      getPreviousNext(listSorted, nakedJid, (current, previous, next) => {
        console.log(previous.pubKey, next.pubKey);
        const zPrevious = cryptoHelpers.hashData(dh.computeSecret(previous.pubKey), uid);
        const zNext = cryptoHelpers.hashData(dh.computeSecret(next.pubKey), uid);
        try {
          cryptoHelpers.xorBuffers([zPrevious, zNext], (value) => {
            current.z = value;
            current.d = zNext;
            current.signature = cryptoHelpers.sign(participantjid, current.z, uid, current.signaturePrivKey);
            current.status = ParticipantStatus.PARAMETERS_CONFIGURED;
            callback(current.z, uid, current.signature, chatrooms[chatroom]);
          });
        } catch (e) {
          console.error(e);
        }
      });
    }
  };

  /**
   * Set second phase parameters on each participant.
   * @param {String} chatroom The chatroom that key gen is perfomed.
   * @param {String} nick The participant nick.
   * @param {String} uid The session id.
   * @param {String} z The x parameter calculates on each participant
   * @param {String} signature The cryptographic signature generated from user's jid,uid and z.
   */
  this.setParamsFromSecondStep = (chatroom, nick, uid, z, signature, callback) => {
    
    return this.getParticipantByNick(chatroom, nick, (friend) => {
      if (!cryptoHelpers.verify(friend.jid, z, uid, signature, friend.signatureKey)) {
        friend.status = ParticipantStatus.SIGNATURE_ABORTED;
        return callback(new Error('Signature not Verified'));
      } 
      friend.z = z;
      friend.signature = signature;
      friend.status = ParticipantStatus.PARAMETERS_CONFIGURED;
      const allParticipansHaveSameStatus = _.reduce(chatrooms[chatroom].friends, (memo, iteratedfriend) => {
       return memo && iteratedfriend.status === ParticipantStatus.PARAMETERS_CONFIGURED
      }, true);
      // console.log("Signature Verified", allParticipansHaveSameStatus);
      if (allParticipansHaveSameStatus) return callback(null, true); 
      return callback(null, false);
    });
  };

  /**
   * @param {String} chatroom The chatroom Name
   * @param {String} key The metadata Information
   * @param {*} value The value for the metadata.
   */
  this.setRoomMetadata = (chatroom, key, value) => {
    if (key === 'friends') throw new Error('Should not place as key \'friends\' to chatroom storage object as metadata');
    chatrooms[chatroom][key] = value;
  };

  this.getChatroomMetadata = (chatroom, key) => chatrooms[chatroom][key];
  
  const generateBuffer = (value) => {
    if(Buffer.isBuffer(value)) return value;
    return Buffer.from(value, 'hex');
  };

  const calculateTempValuesforSecretKey = (listSorted, participantJid, uid, callback) => {
    getPreviousNext(listSorted, participantJid, (current, previous, next) => {
      if (!_.has(next, 'd')) {
        return cryptoHelpers.xorBuffers([generateBuffer(current.d), generateBuffer(next.z)], (value) => {
          next.d = value;
          calculateTempValuesforSecretKey(listSorted, next.jid, uid, callback);
        });
      }
      const buffers = _.map(listSorted, participant => participant.d);
      buffers.push(generateBuffer(uid));
      _.each(buffers, buffer => buffer.toString('hex'));
      return callback(cryptoHelpers.hashBufferList(buffers));
    });
  };

  this.generateFinalKey = (chatroom, participantJid, uid, callback) => {
    const listSorted = _.sortBy(chatrooms[chatroom].friends, 'nick');
    calculateTempValuesforSecretKey(listSorted, participantJid, uid, callback);
  };
}

module.exports.ParticipantStatus = ParticipantStatus;
module.exports.ChatRoomStatus = ChatRoomStatus;
