/**
 * Plugin that actuallt is a switch that toggles the classes from data- attributes
 */
const jQuery = require('jquery');

(function($){
    $.fn.changeClass = (bool) => {
        const dataSuccessClasses = this.attr('data-success');
        const dataFailClasses = this.attr('data-fail');

        if(dataSuccessClasses && dataFailClasses){
            if(bool){
                this.removeClass(dataFailClasses);
                this.addClass(dataSuccessClasses);
            } else {
                this.removeClass(dataSuccessClasses);
                this.addClass(dataFailClasses);
            }
        }
        return this;
    };
}(jQuery));