if(!window.$ || !window.jQuery){
 window.$ = window.jQuery = require('../../node_modules/jquery/dist/jquery.min.js');
}

module.exports = () => {
  $('*[data-page="container"] *[data-page="page"]').on('page-change', (e, preCallback, postCallback) => {
    const target = e.target;

    if (preCallback) {
      preCallback(target);
    }

    $('*[data-page="container"] *[data-page="page"]').not(target).attr('data-page-status', 'destroyed');
    $('*[data-page="container"] *[data-page="page"]').not(target).trigger('page-destroy');

    $('*[data-page="spinner"').show();
    $(target).attr('data-page-status', 'loading');

    $(target).trigger('page-init', [ (err) => {
      if (!err) {
          console.log('Shown');

          $(target).removeAttr('data-page-status');
          $('*[data-page="spinner"').hide();
          if (postCallback) {
           postCallback(null, target);
         }
        } else {
          console.error('Error Occured during page load:', err);
          if (postCallback) {
              postCallback(error, target);
            }
        }
    }]);
  });
}