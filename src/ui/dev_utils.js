module.exports.handelbars_dev_utils = (handlebars) => {
    handlebars.registerHelper('debug', (optionalValue) => {
        console.log('Current Context');
        console.log('====================');
        console.log(this);
      
        if (optionalValue) {
          console.log('Value');
          console.log('====================');
          console.log(optionalValue);
        }
      });
      
};