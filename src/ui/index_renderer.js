// Jquery Based utils
if (typeof module === 'object') { window.module = module; module = undefined; }
window.$ = window.jQuery = require('jquery');
require('popper.js');
require('bootstrap');
// require('./classSwitch');
const pager = require('./no_url_pager');

if (window.module) module = window.module;

// electon libs and depedencies
const ipcRenderer = require('electron').ipcRenderer;
const isDev = require('electron-is-dev');

// Other depedencies
const handlebars = require('handlebars');
const _ = require('underscore');

// Templating
if (isDev) {
  const { handelbars_dev_utils } = require('./dev_utils');
  handelbars_dev_utils(handlebars);
}

handlebars.registerHelper('removeSpaces', value => value.replace(' ', '').replace('#', '').replace('"', ''));

/**
 * Variables defined here because is used by template callbacks defined oustside document.ready
 */
let rosterTemplateRenderer = null;
let friendRequestTemplate = null;
let keyAgreementFriendTemplate = null;

/**
 * Load a Handlebars template
 * @param {String} id
 */
const loadTemplate = (id) => {
  const source = $(id).html();
  return handlebars.compile(source);
};

/**
 * Load the template for the roster
 */
const loadRosterTemplate = () => loadTemplate('#roster_template');

/**
 * Load the template for the friendRequest
 */
const loadFriendRequestTemplate = () => loadTemplate('#friendRequest');

/**
 * Template that renders a friend for a key agreement
 */
const loadKeyAgreementFriendTemplate = () => loadTemplate('#keyAgreementFriend');

// UI

/**
 * Code provided when we need to empty the friends page
 */
const deInitFriendsPage = () => {
  $('#friendRequestsWrapper').hide();
  $('#friendRequests').empty();
  $('#roster').empty();
  $('#key_agree').hide();
};

// Callback function that removes an element.
const removeElement = (elem) => {
  $(elem).remove();
};

const addFoKeyAgreement = (elem) => {
  const jid = $(elem).attr('data-jid');
  const status = $(elem).attr('data-status');
  if ($('#key_agreement_participants').find(`li[data-jid='${jid}']`).length === 0) {
    const html = keyAgreementFriendTemplate({ jid, status });
    $('#key_agreement_participants').append(html);
    areParticipantsOnlineForKeyAgreement();
  }
};

//Chatroom

const onRoomCreation = (event, created, chatroomName) => {

};

// misc functions

/**
 * Enables the key agreement button
 */
const areParticipantsOnlineForKeyAgreement = () => {
  const elements = $('#key_agreement_participants').children();
  const ok = elements.length !== 0 && _.reduce(elements, (memo, elem) => memo && $(elem).attr('data-status') !== 'offline', true);

  if (ok) {
    $('#key_agree').show();
  } else {
    $('#key_agree').hide();
  }
};

/**
 * Load the roster
 * @param {Event} event
 * @param {Object} data
 */
const rosterLoader = (event, data) => {
  const renderValues = { roster: data };
  const generatedHtml = rosterTemplateRenderer(renderValues);
  $('#roster').html(generatedHtml);

  _.forEach(data, (participants) => {
    console.log(participants);
    _.forEach(participants, (participant) => {
      console.log(participant);
      $('#key_agreement_participants').find(`.participant[data-jid="${participant.jid}"]`).attr('data-status', participant.status);
    });
  });

  areParticipantsOnlineForKeyAgreement();

  ipcRenderer.send('triggerOnline');
};

/**
 * Removes a friend from request list.
 * @param {String} jid The xmpp id of the friend.
 */
const emptyFriendRequests = (jid) => {
  $(`*[data-friend-request-jid="${jid}"]`).remove();
  if ($('#friendRequests').children().length === 0) {
    $('#friendRequestsWrapper').hide();
  }
};


/**
 * Functionality when a friend request happened
 * @param {Event} event
 * @param {String} jid
 */
const friendRequestLoader = (event, jid) => {
  const renderValues = { jid };
  const generatedHtml = friendRequestTemplate(renderValues);
  $('#friendRequests').append(generatedHtml);
  $('#friendRequestsWrapper').show();

  $('button[data-accept-friend]').off('click').on('click', function (e) {
    const participant = $(this).attr('data-accept-friend');
    ipcRenderer.send('acceptFriend', participant);
    $(this).focusout();
    emptyFriendRequests(participant);
  });

  $('button[data-reject-friend]').off('click').on('click', function (e) {
    const participant = $(this).attr('data-reject-friend');
    ipcRenderer.send('rejectFriend', participant);
    $(this).focusout();
    emptyFriendRequests(participant);
  });
};


$(document).ready(() => {
  $('#loginForm').on('submit', function (e) {
    e.preventDefault();

    // https://stackoverflow.com/a/29000408/4706711
    const data = $(this).serializeArray().reduce((a, x) => { a[x.name] = x.value; return a; }, {});
    ipcRenderer.send('login', data);

    ipcRenderer.once('disconnected', (em, status, reason) => {
      if (status) {
        $('*[data-page-options="default"]').trigger('page-change');
      }
      console.log('Disconnected', status, reason);
    });
  });

  // Code that is executed when login is sucessfull
  ipcRenderer.on('login_ok', (e, address) => {
    $('#logedinXmppUserName').text(address);
    $('#friendsPage').trigger('page-change');
  });

  $('#logout').on('click', (e) => {
    e.preventDefault();
    ipcRenderer.send('logout');
  });


  // Page definitions
  $('#loginPage').on('page-init', (e, callback) => {
    deInitFriendsPage();
    setTimeout(() => {
      callback(null);
    }, 1000);
  });

  $('#friendsPage').on('page-init', (e, callback) => {
    // Bootstrap Roster Listeners
    if (!rosterTemplateRenderer) {
      rosterTemplateRenderer = loadRosterTemplate();
    }

    if (!friendRequestTemplate) {
      friendRequestTemplate = loadFriendRequestTemplate();
    }

    if (!keyAgreementFriendTemplate) {
      keyAgreementFriendTemplate = loadKeyAgreementFriendTemplate();
    }

    ipcRenderer.on('roster', rosterLoader);
    ipcRenderer.on('friendRequest', friendRequestLoader);

    // Trigger Key Agreement
    $('#key_agree').off('click').on('click', (e) => {
      e.preventDefault();
      const jids = _.map($('#key_agreement_participants').children(), (element, key) => $(element).attr('data-jid'));
      ipcRenderer.send('key_agreement', jids);
      // ipcRenderer.on('roomCreation')
    });

    callback(null);
  });


  pager();

  // Sync ZONE
  const address = ipcRenderer.sendSync('isLogedIn');
  if (address) {
    $('#logedinXmppUserName').text(address);
    $('#friendsPage').trigger('page-change');
  } else {
    $('*[data-page-options="default"]').trigger('page-change');
  }
});
