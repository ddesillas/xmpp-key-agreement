const crypto = require('crypto');

const k1 = crypto.createDiffieHellman(2048);
const p = k1.getPrime();
const g = k1.getGenerator();
console.log(Buffer.isBuffer(p), Buffer.isBuffer(g));