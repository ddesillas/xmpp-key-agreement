const { xorBuffers } = require('../src/keygen_helpers');

const buffers = [Buffer.from([0x00000, 0x11111]), Buffer.from([0x11111, 0x00000])];
xorBuffers(buffers, ( finbalBuffer ) => {
  console.log(finbalBuffer.toString('hex'));
});