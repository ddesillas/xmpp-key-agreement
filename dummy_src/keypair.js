const keypair = require('keypair');
const crypto = require('crypto');

const pair = keypair();
console.log(pair.public, pair.private);

// Create Transmitted Signature
const sign = crypto.createSign('RSA-SHA256');
sign.write('abcdef');  // data from your file would go here
sign.write('1234');
sign.end();
const sig = sign.sign(pair.private, 'hex');
console.log(sig);

// Verifying Signature
const verify = crypto.createVerify('RSA-SHA256');
verify.write('abcdef');
verify.write('1234');
verify.end();
console.log(verify.verify(pair.public, sig,'hex'));