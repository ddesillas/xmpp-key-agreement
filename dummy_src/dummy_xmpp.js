const { client } = require('@xmpp/client');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

const initXmpp = function (xmpp) {
  
  xmpp.on('error', (err) => {
    console.error('Error occured', err.toString());
  });

  xmpp.on('offline', () => {
    console.log('🛈', 'offline');
  });

  xmpp.on('online', (address) => {
    console.log(address);
  });

  xmpp.once('open', (el) => {
    console.log(el);
  });

  xmpp.on('stanza', (stanza) => {
    console.log('⮈', stanza.toString());
  });

  xmpp.on('status', (status) => {
    console.debug('🛈', 'status', status);
  });

  xmpp.on('input', (input) => {
    console.debug('⮈', input);
  });

  xmpp.on('output', (output) => {
    console.debug('⮊', output);
  });

  xmpp.on('stanza', (stanza) => {
    console.log('⮈', stanza.toString());
  });

  xmpp.start().then((value) => {
    console.log('Value', value);
  }).catch((e) => {
    console.error('Error Occured', e);
  });
};


const clientInstance = new client({
  service: 'xmpp://0.0.0.0:5222',
  domain: 'example.com',
  username: 'admin',
  password: 'admin',
});

initXmpp(clientInstance);

process.on('unhandledRejection', (reason, p) => {
  console.error('Possibly Unhandled Rejection at: Promise ', p, ' reason: ', reason);
});