const Test = require('tap');
const _ = require('underscore');
const { xorBuffers, xoredBuffersAreZero } = require('../src/keygen_helpers');

Test.test('xorBuffers', (t) => { 
  const buffers = [
    Buffer.from([ parseInt('000000000', 2), parseInt('11111111', 2) ]),
    Buffer.from([ parseInt('11111111', 2), parseInt('000000000', 2) ]),
  ];
  const expected = Buffer.from([ parseInt('11111111', 2), parseInt('11111111', 2)]);
  xorBuffers(buffers, ( finbalBuffer ) => {
    t.equal(finbalBuffer.toString('hex'), expected.toString('hex'));
    t.end();
  });
});

Test.test('xoredBuddersAreZero', (t) => {
  const buffers = [
    Buffer.from([ parseInt('000000000', 2), parseInt('11111111', 2) ]),
    Buffer.from([ parseInt('000000000', 2), parseInt('11111111', 2) ]),
  ];
  xoredBuffersAreZero(buffers, (value) => {
    if(value) t.ok('XORing buffers result 0');
    else t.notOk('XORing buffers DOES NOT result 0');
    t.end();
  });
});

Test.test('multi Buffer Xor', (t) => {

  const buffers = [
    Buffer.from([ parseInt('000000000', 2), parseInt('11111111', 2) ]),
    Buffer.from([ parseInt('11111111', 2), parseInt('000000000', 2) ]),
    Buffer.from([ parseInt('11111111', 2), parseInt('000000000', 2), parseInt('11111111', 2) ]),
  ];

  xorBuffers(buffers, (finalBuffer) => {
    console.log("MultiXor", finalBuffer.toString('hex'));
    t.ok('Message Ok');
    t.end();
  });

});