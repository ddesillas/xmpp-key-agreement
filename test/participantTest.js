const Test = require('tap');
const { RosterItem } = require('../src/xmpp_helpers/roster');

Test.test('createItem', (t) => {
    const participant = new RosterItem('user@example.com');
    t.equal(participant.getJid(), 'user@example.com');
    t.end();
});